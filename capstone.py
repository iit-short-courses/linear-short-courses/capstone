
# 1. Write a python statements that will satisfy the following situations and requirements:

	# a. Create a "numbers" list that will perform the following operations:
		# items that should be in the list: 5,11,17,19,25,29,30,30,32,46,90
print("--------------------Activity 1a-------------------------")

numbers = [5, 11, 17, 19, 25, 29, 30, 30, 32, 46, 90]
print(len(numbers))
print(numbers.count(30))
numbers.remove(17)
print(numbers)

print("\n")

print("--------------------Activity 1b-------------------------")

numbers = [5, 11, 17, 19, 25, 29, 30, 30, 32, 46, 90]

sumOfEven = 0
sumOfOdd = 0

for i in range(len(numbers)):
	if(numbers[i] % 2 == 0):
		sumOfEven = sumOfEven + numbers[i]
	else:
		sumOfOdd = sumOfOdd + numbers[i]

print("Sum of all Even and Odd items in the numbers list:")
print(f"Sum of Even numbers: {sumOfEven}")
print(f"Sum of Odd numbers: {sumOfOdd}")

print("\n")

print("--------------------Activity 1c-------------------------")

visitors = []

def PUSH(name):
	visitors.append(name) 
	top = len(visitors)-1

def POP():
	if len(visitors) == 0:
		return "Sorry! No Visitor to delete!"
	else:
		val = visitors.pop() 
		if len(visitors) == 1: 
			top = None 
		else:
			top = len(visitors) - 1
		return val



print(f"\nCurrent content of visitors: {visitors}")
print(f"Current length of the visitors: {len(visitors)}")
print("Invoking the POP method:")

print(POP())
PUSH("John")

print(f"\nUpdated visitors list after using PUSH('John') Method: {visitors}")
print(f"Current length of visitors list after using PUSH('John') Method: {len(visitors)}")
print("Invoking the POP method:")
print(POP())
print("Invoking the POP method again:")
print(POP())
print(f"Updated visitors list after using POP() Method: {visitors}")

print("--------------------Activity 1d-------------------------")

requestsNo = []

def enqueue(item):
	requestsNo.append(item) 
	if len(requestsNo) == 0: 
		front = 0
		rear = 0
	else:
		rear = len(requestsNo)-1
	return requestsNo

def dequeue():
	if len(requestsNo) == 0:
		return "Underflow"
	else:
		val = requestsNo.pop()
	if len(requestsNo) == 0: 
		front = None
		rear = None 
	return val

print(f"\nCurrent content of requestsNo: {requestsNo}")
print(f"Current length of the requestsNo: {len(requestsNo)}")
print("Invoking the dequeue() method: ")
print(dequeue())

enqueue("RQN001")
enqueue("RQN002")

print(f"\nCurrent content of requestsNo after enqueue() method: {requestsNo}")
print(f"Current length of the requestsNo after enqueue()method : {len(requestsNo)}")
print("Invoking the dequeue() method: ")
print(dequeue())
print(f"Current content of requestsNo after dequeue() method: {requestsNo}")
print("\n")

print("--------------------Activity 1e-------------------------")

roman_numerals = {'I': 1, 'II': 2, 'III': 3, 'V': 5}

print(list(roman_numerals.keys()))
print(list(roman_numerals.values()))
print(roman_numerals)

